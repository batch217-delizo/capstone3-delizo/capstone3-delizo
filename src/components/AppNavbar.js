import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from "react-router-dom";
const AppNavbar = () => {

  return (
    <Navbar bg="primary" variant="dark" className="AppNav justify-content-center " >
      <Nav activeKey="/home">
        <Nav.Item>
          <Nav.Link as={NavLink} to="/laptop">laptop</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link as={NavLink} to="/desktop">Desktop</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link as={NavLink} to="/peripheral">Peripherals</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link as={NavLink} to="/contactus">Contact US</Nav.Link>
        </Nav.Item>
      </Nav>
    </Navbar>
  )

}


export default AppNavbar