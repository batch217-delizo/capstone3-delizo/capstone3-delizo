
import Showcase from "../components/Showcase";
import Services from "../components/Services"

const Home = () => {


  return (
    <>

      <div className="showcase-Container" >
        <Showcase style={{ fontSize: "2rem", color: "#fff" }} />
        <Services />
      </div>

    </>
  )
}

export default Home